//////////////////////////////////////////
///////////    Dependencies   ////////////
//////////////////////////////////////////

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    minifyCss   = require('gulp-minify-css'),
    minifyHTML  = require('gulp-minify-html'),
    server      = require('gulp-server-livereload');

gulp.task('js', function(){

    gulp.src('*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('web/js/'))

});

gulp.task('css', function(){

    return gulp.src('*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('web/css/'))

});

gulp.task('html', function() {

  var opts = {
    conditionals: true,
    spare:true
  };

  return gulp.src('*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('web/html/'))

});

gulp.task('server', function() {
  gulp.src('./')
    .pipe(server({
      livereload: true,
      // directoryListing: true,
      open: true
    }));
});

gulp.task('build', ['js', 'css', 'html']);

//////////////////////////////////////////
///////////         END        ///////////
//////////////////////////////////////////