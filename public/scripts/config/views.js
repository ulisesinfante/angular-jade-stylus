angular.module('myApp', ['ngRoute'])

  .config(function($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'index.html'
        })
        .when('/ruta', {
          controller: 'ControladorName',
          templateUrl: 'file.html'
        })
        .otherwise({
          redirectTo: '/'
        });
    });